package Principal;

import java.util.Random;
import java.util.Scanner;

import Personagem.Mage;
import Personagem.Personagem;
import Personagem.Warrior;
import Power.Focus;
import Power.Fury;

public class Inicio {

	public Inicio(){
		Personagem p1 = null,p2 = null;
		System.out.println("Personagem 1: \n1-Mago\n2-Warrior");
		Scanner s = new Scanner(System.in);
		int tipo = s.nextInt();
		switch(tipo){
			case 1: p1 = new Mage(); break;
			case 2: p1 = new Warrior(); break;
		}
		System.out.println("Personagem 2: \n1-Mago\n2-Warrior");
		tipo = s.nextInt();
		switch(tipo){
		case 1: p2 = new Mage(); break;
		case 2: p2 = new Warrior(); break;
		}
		Random r = new Random();
		int chance = 0, rod = 0, i = 1;
		while((p1.getLife() != 0) && (p2.getLife() != 0)){
			System.out.println("Rodada "+i);
			//powers
			chance = r.nextInt(101);
			if(chance > 60){
				chance = r.nextInt(2);
				if(chance == 0){
					p1 = new Fury(p1);
					System.out.println("Personagem 1 ganhou Fury");
				}else{
					p1 = new Focus(p1);
					System.out.println("Personagem 1 ganhou Focus");
				}
			}else{
				System.out.println("Personagem 1 n�o ganhou power!");
			}
			chance = r.nextInt(101);
			if(chance > 60){
				chance = r.nextInt(2);
				if(chance == 0){
					p2 = new Fury(p2);
					System.out.println("Personagem 2 ganhou Fury");
				}else{
					p2 = new Focus(p2);
					System.out.println("Personagem 2 ganhou Focus");
				}
			}else{
				System.out.println("Personagem 2 n�o ganhou power!");
			}
			//Come�a os ataque
			System.out.println("HP antes do ataques: P1 - "+p1.getLife()+" / P2 - "+p2.getLife());
			System.out.println("Personagem 1: Atk: "+p1.getAtk()+" Def: "+p1.getDef());
			System.out.println("Personagem 2: Atk: "+p2.getAtk()+" Def: "+p2.getDef());
			p2 = p1.ataque(p2);
			p1 = p2.ataque(p1);
			rod = p1.debitaRodada();
			if(rod == 0)p1 = p1.getDecorator();
			rod = p2.debitaRodada();
			if(rod == 0)p2 = p2.getDecorator();
			i++;
			System.out.println("HP pos ataques: P1 - "+p1.getLife()+" / P2 - "+p2.getLife());
		}
		
		if(p1.getLife() != 0){
			System.out.println("Personagem 1 ganhou");
		}else{
			if(p2.getLife() != 0){
				System.out.println("Personagem 2 ganhou");
			}else{
				System.out.println("Empate!");
			}
		}
	}
	
}
