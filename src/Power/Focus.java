package Power;

import Personagem.Personagem;

public class Focus extends Personagem{

	private Personagem p;
	private int rodadas;
	
	public Focus(Personagem p){
		super();
		this.p = p;
		rodadas = 3;
	}
	
	@Override
	public Personagem ataque(Personagem p) {
		p.damage(this.getAtk(),p.getDef());
		return p;
	}

	@Override
	public void damage(Double dano, Double defesa) {
		p.damage(dano,defesa);		
	}

	@Override
	public double getAtk() {
		return p.getAtk();
	}

	@Override
	public double getDef() {
		return p.getDef()*2;
	}

	@Override
	public double getLife() {
		return p.getLife();
	}

	@Override
	public Personagem getDecorator() {
		return p;
	}

	@Override
	public int debitaRodada() {
		rodadas --;
		int r = p.debitaRodada();
		if(r == 0){
			p = p.getDecorator();
		}
		return rodadas;
		
	}

}
