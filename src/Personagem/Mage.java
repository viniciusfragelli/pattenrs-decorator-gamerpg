package Personagem;

public class Mage extends Personagem{

	public Mage(){
		atk = 350;
		def = 100;
		life = 500;
	}
	
	@Override
	public Personagem ataque(Personagem p) {
		p.damage(getAtk(),p.getDef());
		return p;
	}

	@Override
	public void damage(Double dano, Double defesa) {
		dano = dano - defesa;
		if(dano < 0){
			dano = (double) 0;
		}
		System.out.println("Dano recebido: "+dano);
		life = life - dano;
		if(life < 0){
			life = (double) 0;
		}
		
	}

	@Override
	public double getAtk() {
		return atk;
	}

	@Override
	public double getDef() {
		return def;
	}

	@Override
	public double getLife() {
		return life;
	}

	@Override
	public Personagem getDecorator() {
		return this;
	}

	@Override
	public int debitaRodada() {
		return -1;
	}

}
