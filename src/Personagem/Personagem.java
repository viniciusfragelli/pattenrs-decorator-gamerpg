package Personagem;

public abstract class Personagem {
	protected double atk;
	protected double def;
	protected double life;
	
	public abstract Personagem ataque(Personagem p);
	public abstract void damage(Double dano, Double defesa);
	public abstract double getAtk();
	public abstract double getDef();
	public abstract double getLife();
	public abstract Personagem getDecorator();
	public abstract int debitaRodada();
	
}
